```
Thông thường khi làm việc với kubernetes ta sẽ có 2 role là:
- kubernetes administrator: người dựng và quản lý kubernetes cluster, cài những plugin và addons cần thiết cho kubernetes cluster.
- kubernetes developer: người mà sẽ viết file config yaml để deploy ứng dụng lên trên kubernetes.

```

```
Pod (resource run application)
ReplicaSet (resource hỗ trợ cho high availability application)
```

```
Deployment: Recreate & RollingUpdate & Blue-Green update
Deployment tạo và quản lý ReplicaSet -> ReplicaSet tạo và quản lý Pod -> Pod run container.
```

```
API Server:
<api-server-url>/api/v1/namespaces/<namespace-name>/pods
curl 127.0.0.1:8001/api/v1/namespaces/default/pods

Lấy thông tin của 1 Pod:
<api-server-url>/api/v1/namespaces/<namespace-name>/pods/<pod-name>
```
