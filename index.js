// k8s-series
// const http = require("http");

// const server = http.createServer((req, res) => {
//   res.end("Kubernetes Series\n")
// });

// server.listen(3000, () => {
//   console.log("Server listen on port 3000")
// })

// k8s-redis
// const http = require("http");
// const redis = require("redis");

// const client = redis.createClient("redis://redis:6379")

// client.connect().then(() => {
//   console.log("connect redis successfully");
// })

// const server = http.createServer((req, res) => {
//   console.log("redis app\n")
// });

// server.listen(3000, () => {
//   console.log("Server listen on port ", 3000)
// })


// k8s-env
// const http = require("http");
// const { Client } = require("pg");

// const { DB_HOST, POSTGRES_USER, POSTGRES_DB, POSTGRES_PASSWORD } = process.env;

// const client = new Client({
//   host: DB_HOST,
//   user: POSTGRES_USER,
//   database: POSTGRES_DB,
//   password: POSTGRES_PASSWORD,
//   port: 5432,
// });
// client.connect().then(() => {
//   console.log("connect db successfully");
// })

// const server = http.createServer((req, res) => {
//   console.log("env app\n")
// });

// server.listen(process.env.PORT, () => {
//   console.log("Server listen on port ", process.env.PORT)
// })

// StatefulSets
const http = require('http');
const os = require('os');
const fs = require('fs');

const dataFile = "/var/data/kubia.txt";

function fileExists(file) {
  try {
    fs.statSync(file);
    return true;
  } catch (e) {
    return false;
  }
}

var handler = function(request, response) {
  if (request.method == 'POST') {
    var file = fs.createWriteStream(dataFile);
    file.on('open', function (fd) {
      request.pipe(file);
      console.log("New data has been received and stored.");
      response.writeHead(200);
      response.end("Data stored on pod " + os.hostname() + "\n");
    });
  } else {
    var data = fileExists(dataFile) ? fs.readFileSync(dataFile, 'utf8') : "No data posted yet";
    response.writeHead(200);
    response.write("You've hit " + os.hostname() + "\n");
    response.end("Data stored on this pod: " + data + "\n");
  }
};

var www = http.createServer(handler);
www.listen(8080);
